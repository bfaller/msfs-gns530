# MSFS GNS530 VLOC Ident Window Fix

This fixes the wrong radial information and the rounded DME in the GNS530.

Please send file a bug report and point them to this fix. They only have to
change a few lines of code.

## Installation

Copy the `gns530` folder to the `Community` Folder.
