# Changelog

## 0.1.1

- Fixed display for radials between 181 and 359 degrees

## 0.1.0

- Initial release
